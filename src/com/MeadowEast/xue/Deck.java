package com.MeadowEast.xue;

import android.annotation.SuppressLint;
import java.util.LinkedList;

@SuppressLint("NewApi")
// Queue
public class Deck {
	private LinkedList<CardStatus> cardStatusQueue = new LinkedList<CardStatus>();
	public Deck() {}
	public CardStatus get(){ 
		return cardStatusQueue.poll();
	}
	public void put(CardStatus cs){ // add CardStatus to end
		cardStatusQueue.add(cs);
	}
	public boolean isEmpty(){
		return cardStatusQueue.isEmpty();
	}	
	public int size(){
		return cardStatusQueue.size();
	}
	public CardStatus top(){ // return first element without deleting it
		return cardStatusQueue.getFirst();
	}
	public void insertInHead(CardStatus cs){ // insert CardStatus in the beginning
		cardStatusQueue.addFirst(cs);		 // necessary for undo - to see the card right away
	}
	public boolean contains(CardStatus cs){
		return cardStatusQueue.contains(cs);
	}
	public void removeDuplicate(CardStatus cs){ // card can be in deck more than once if wrong() was called a lot
		cardStatusQueue.remove(cardStatusQueue.indexOf(cs)); 
	}
}
