package com.MeadowEast.xue;

import android.annotation.SuppressLint;
import java.util.LinkedList;

@SuppressLint("NewApi")
// Stack
public class undoDeck { 
	private LinkedList<CardStatus> cardStatusQueue = new LinkedList<CardStatus>();
	public undoDeck() {}
	public void push(CardStatus cs){ // add CardStatus to end
		cardStatusQueue.add(cs);
	}
	public boolean isEmpty(){
		return cardStatusQueue.isEmpty();
	}	
	public int size(){
		return cardStatusQueue.size();
	}
	public CardStatus pop() { // remove CardStatus from end and return it
		return cardStatusQueue.pollLast();
	}
	public CardStatus last(){ // // return last element without deleting it
		return cardStatusQueue.getLast();
	}
	public boolean contains(CardStatus cs){
		return cardStatusQueue.contains(cs);
	}
}