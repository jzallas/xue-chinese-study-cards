package com.MeadowEast.xue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

//********AsyncTask Class********

public class Download extends AsyncTask<Void, Void, String> {
	
	ProgressDialog mProgressDialog;
	Context context;
	String url;
//	Date lastModDate;
//	Date serverFormattedDate;
	
	public Download(Context context,String url) {
		this.context = context;
		this.url=url;
	}

	protected void onPreExecute() {
		Log.i("onPreExecute()", "");
		
		mProgressDialog = ProgressDialog.show(context, "",	"Please wait, Download …");
	}
	

	protected void onPostExecute(String result) { // should be Void result
		if (result.equals("done")) {
			Log.i("", "done in POSTExecute");
			Toast.makeText(context, "Vocabulary successfully updated",
					Toast.LENGTH_LONG).show();
			mProgressDialog.dismiss();
		}else if(result.equals("none")){
			Log.i("", "DID NOT download");
			Toast.makeText(context, "No update vocabulary file posted",
					Toast.LENGTH_LONG).show();
			mProgressDialog.dismiss();
		}		
	}
	
//	@Override
	protected String doInBackground(Void... params) {
		try {
			URL url = new URL("http://www.meadoweast.com/capstone/vocabUTF8.txt");
			HttpURLConnection c = (HttpURLConnection) url.openConnection();
			c.setRequestMethod("GET");
			c.setDoOutput(false);
			c.connect();
			String[] path = url.getPath().split("/");
			String mp3 = path[path.length - 1];
			int lengthOfFile = c.getContentLength();
			// server
			String lastModified = c.getHeaderField("Last-Modified");
			Date serverFormattedDate = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss zzz", Locale.ENGLISH).parse(lastModified);
			Log.i("SERVER parsed ---> ", ""+ serverFormattedDate);
			// local			
			File file1 = new File("/storage/sdcard0/Android/Data/com.MeadowEast.xue/files/vocabUTF8.txt");
			Date lastModDate = new Date(file1.lastModified());
			
			Date localFormattedDate = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy", Locale.ENGLISH).parse(lastModDate.toString());
			Log.i("LOCAL parsed--->", ""+localFormattedDate);
			if(!serverFormattedDate.after(localFormattedDate)){ // if server file was not updated and is still older
				Log.i("", "No download needed");
				return "none";
			}
			
			String PATH = Environment.getExternalStorageDirectory()+ "/Android/Data/com.MeadowEast.xue/files/" ;
			File file = new File(PATH);
			file.mkdirs();
			String fileName = mp3; // rename this
			
			File outputFile = new File(file , fileName);
			
			FileOutputStream fos = new FileOutputStream(outputFile);
			InputStream is = c.getInputStream();
			byte[] buffer = new byte[1024];
			int len1 = 0;
			while ((len1 = is.read(buffer)) != -1) {
				fos.write(buffer, 0, len1);
			}
			fos.close();
			is.close();
		} catch (IOException e) {
				e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.i("done in background", "");
		return "done";
	}
	
}