package com.MeadowEast.xue;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.*;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RelativeLayout;

// ******Activity Class*************
public class downloadActivity extends Activity  {
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		loadSettings();
		new Download(downloadActivity.this, "http://www.meadoweast.com/capstone/vocabUTF8.txt").execute();
	}
	
	private void loadSettings() {
		SharedPreferences sharedPref = PreferenceManager
				.getDefaultSharedPreferences(this);
		RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.MainLayout);
		mainLayout.setBackgroundColor(Color.parseColor(sharedPref
				.getString("bg_color",
						getResources().getString(R.color.background_color5))));
		}
}

 

