package com.MeadowEast.xue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class StatisticsActivity extends Activity implements OnClickListener {
	static final String EC = "EnglishChinese";
	static final String CE = "ChineseEnglish";
	Button ecStats, ceStats;
	ImageView[] barGraph;
	TextView[] cardMessage;
	TextView statDateMessage, statAvg, progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics);
		// Show the Up button in the action bar.
		setupActionBar();
		connectUIElements();
		loadSettings();
		displayStats(EC);
	}

	private void loadSettings() {
		// SET BACKGROUND COLOR
		SharedPreferences sharedPref = PreferenceManager
				.getDefaultSharedPreferences(this);
		RelativeLayout statLayout = (RelativeLayout) findViewById(R.id.statLayout);
		statLayout.setBackgroundColor(Color.parseColor(sharedPref
				.getString("bg_color",
						getResources().getString(R.color.background_color5))));
	}

	private void connectUIElements() {
		// top buttons
		ecStats = (Button) findViewById(R.id.ecStats);
		ecStats.setOnClickListener(this);
		ceStats = (Button) findViewById(R.id.ceStats);
		ceStats.setOnClickListener(this);

		// bargraph elements
		barGraph = new ImageView[5];
		barGraph[0] = (ImageView) findViewById(R.id.L0Bar);
		barGraph[1] = (ImageView) findViewById(R.id.L1Bar);
		barGraph[2] = (ImageView) findViewById(R.id.L2Bar);
		barGraph[3] = (ImageView) findViewById(R.id.L3Bar);
		barGraph[4] = (ImageView) findViewById(R.id.L4Bar);

		// individual card class status
		cardMessage = new TextView[5];
		cardMessage[0] = (TextView) findViewById(R.id.L0Message);
		cardMessage[1] = (TextView) findViewById(R.id.L1Message);
		cardMessage[2] = (TextView) findViewById(R.id.L2Message);
		cardMessage[3] = (TextView) findViewById(R.id.L3Message);
		cardMessage[4] = (TextView) findViewById(R.id.L4Message);

		// general descriptions
		statDateMessage = (TextView) findViewById(R.id.statDateMessage);
		statAvg = (TextView) findViewById(R.id.statAvg);
		progress = (TextView) findViewById(R.id.progress);
	}

	// checks if a string containing a date in the format MM/dd/yy is within the
	// last week of the current date
	private boolean isWithinLastWeek(String date) {
		Date logDate, now;
		try {
			now = new Date();
			logDate = new SimpleDateFormat("MM/dd/yy", Locale.ENGLISH)
					.parse(date);

			if (date.length() == 8) {
				long differenceInMillis = now.getTime() - logDate.getTime();
				if (differenceInMillis <= TimeUnit.MILLISECONDS.convert(7,
						TimeUnit.DAYS))
					return true;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return false;
		}
		return false;
	}

	// returns array of length 14 containing the following information to
	// display:
	// date, time, L0, L0weight, L1, L1weight, L2, L2weight, L3, L3weight, L4,
	// L4weight, week, avg
	private void calculateSetStats(String filename) {
		File file = new File(MainActivity.filesDir, filename);
		int lastWeek = 0; // the number of decks that were recorded from the
							// last week
		float avg = 0; // holds the average per day
		int total = 0; // the sum of : the number of cards learned in each
						// session
		String lastLine = ""; // contains the last line of the file
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));
			String line = in.readLine();
			Map<String, Integer> learnedPerDay = new HashMap<String, Integer>();
			while (line != null) {
				String fixedline = new String(line.getBytes(), "utf-8");
				String[] fields = fixedline.split("\\s+"); // delimit by
															// whitespace
				if (fields.length == 14) {
					if (isWithinLastWeek(fields[0])) {
						lastWeek++;
						learnedPerDay.put(fields[0],
								Integer.parseInt(fields[12]));
					}
				} else {
					Log.d("StatisticsActivity", "Bad line: " + fields.length
							+ " elements");
					Log.d("StatisticsActivity", fixedline);
				}
				lastLine = line;
				line = in.readLine();
			}
			Object[] keys = learnedPerDay.keySet().toArray();
			Arrays.sort(keys);
			int prevValue = 0;
			for (Object key : keys) {
				total = total + (learnedPerDay.get(key) - prevValue);
				prevValue = learnedPerDay.get(key);
			}
			avg = (float) total / (float) learnedPerDay.size();
			in.close();
		} catch (Exception e) {
			// UNABLE TO GET LOG HERE
			emptyScreen();
			return;
		}

		// PARSE LAST LINE//

		String[] fields = lastLine.split("\\s+");
		if (fields.length == 14) { // only parse if the line is the right size
			String cardClass[] = new String[5]; // contains total number of each
												// card class
			cardClass[0] = fields[2];
			cardClass[1] = fields[3];
			cardClass[2] = fields[5];
			cardClass[3] = fields[8];
			cardClass[4] = fields[10];

			// percentage of each card class
			float weights[] = getWeights(cardClass);

			// done calculating, now change the display to reflect calculations
			setStats(fields[0], fields[1], cardClass, fields[13], weights,
					lastWeek, avg);
		}
	}

	private float[] getWeights(String cardClass[]) {
		int total = 0;
		float weights[] = new float[cardClass.length];

		// first get the total number of cards
		for (int i = 0; i < cardClass.length; i++) {
			total = total + Integer.parseInt(cardClass[i]);
		}
		// then get the percentage out of the total of each card's class
		for (int i = 0; i < cardClass.length; i++) {
			weights[i] = (float) Integer.parseInt(cardClass[i]) / (float) total;
		}
		return weights;
	}

	private void setStats(String date, String time, String cardClass[],
			String totalCards, float classWeight[], int totalWeeks,
			float average) {
		loadScreen();
		barGraph[0].setVisibility(8);
		ImageView L0Color = (ImageView) findViewById(R.id.L0Color);
		L0Color.setVisibility(8);
		ImageView L0ColorOutline = (ImageView) findViewById(R.id.L0ColorOutline);
		L0ColorOutline.setVisibility(8);
		
		for (int i = 1; i < 5; i++) {
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,
					LayoutParams.MATCH_PARENT, classWeight[i]);
			barGraph[i].setLayoutParams(params);
		}
		statDateMessage.setText("Last deck completed on : " + date + " at "
				+ time);
		if (totalWeeks != 0)
			statAvg.setText("In the last week, you have completed "
					+ Integer.toString(totalWeeks)
					+ " decks and learned an average of "
					+ Float.toString(average) + " cards per day.");
		else
			statAvg.setText("You have not completed any decks within the last week.");

		progress.setText("Progress Summary To Date : ");

		cardMessage[0].setText("");
		//cardMessage[0].setText("Not Yet Seen : " + cardClass[0]);
		cardMessage[1].setText("Barely Learned : " + cardClass[1]);
		cardMessage[2].setText("Currently Learning : " + cardClass[2]);
		cardMessage[3].setText("Almost Learned : " + cardClass[3]);
		cardMessage[4].setText("Already Learned : " + cardClass[4]);

	}

	private void emptyScreen() {
		for (int i = 0; i < 5; i++) {
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,
					LayoutParams.WRAP_CONTENT, 0);
			barGraph[i].setLayoutParams(params);
		}
		ImageView barOutline = (ImageView) findViewById(R.id.barOutline);
		ImageView L0Color = (ImageView) findViewById(R.id.L0Color);
		ImageView L1Color = (ImageView) findViewById(R.id.L1Color);
		ImageView L2Color = (ImageView) findViewById(R.id.L2Color);
		ImageView L3Color = (ImageView) findViewById(R.id.L3Color);
		ImageView L4Color = (ImageView) findViewById(R.id.L4Color);
		ImageView L0ColorOutline = (ImageView) findViewById(R.id.L0ColorOutline);
		ImageView L1ColorOutline = (ImageView) findViewById(R.id.L1ColorOutline);
		ImageView L2ColorOutline = (ImageView) findViewById(R.id.L2ColorOutline);
		ImageView L3ColorOutline = (ImageView) findViewById(R.id.L3ColorOutline);
		ImageView L4ColorOutline = (ImageView) findViewById(R.id.L4ColorOutline);
		barOutline.setVisibility(8);
		L0Color.setVisibility(8);
		L1Color.setVisibility(8);
		L2Color.setVisibility(8);
		L3Color.setVisibility(8);
		L4Color.setVisibility(8);
		L0ColorOutline.setVisibility(8);
		L1ColorOutline.setVisibility(8);
		L2ColorOutline.setVisibility(8);
		L3ColorOutline.setVisibility(8);
		L4ColorOutline.setVisibility(8);
		statDateMessage
				.setText("Please complete a deck first in order to view statistics.");
		statAvg.setText("");
		progress.setText("");

		cardMessage[0].setText("");
		cardMessage[1].setText("");
		cardMessage[2].setText("");
		cardMessage[3].setText("");
		cardMessage[4].setText("");

	}

	private void loadScreen() {
		ImageView barOutline = (ImageView) findViewById(R.id.barOutline);
		ImageView L0Color = (ImageView) findViewById(R.id.L0Color);
		ImageView L1Color = (ImageView) findViewById(R.id.L1Color);
		ImageView L2Color = (ImageView) findViewById(R.id.L2Color);
		ImageView L3Color = (ImageView) findViewById(R.id.L3Color);
		ImageView L4Color = (ImageView) findViewById(R.id.L4Color);
		ImageView L0ColorOutline = (ImageView) findViewById(R.id.L0ColorOutline);
		ImageView L1ColorOutline = (ImageView) findViewById(R.id.L1ColorOutline);
		ImageView L2ColorOutline = (ImageView) findViewById(R.id.L2ColorOutline);
		ImageView L3ColorOutline = (ImageView) findViewById(R.id.L3ColorOutline);
		ImageView L4ColorOutline = (ImageView) findViewById(R.id.L4ColorOutline);
		barOutline.setVisibility(0);
		// L0Color.setVisibility(0);
		L1Color.setVisibility(0);
		L2Color.setVisibility(0);
		L3Color.setVisibility(0);
		L4Color.setVisibility(0);
		// L0ColorOutline.setVisibility(0);
		L1ColorOutline.setVisibility(0);
		L2ColorOutline.setVisibility(0);
		L3ColorOutline.setVisibility(0);
		L4ColorOutline.setVisibility(0);

	}

	private void displayStats(String name) {

		// code to turn on and off buttons so that only one stat can be selected
		// at a time
		if (name.equals(EC)) {
			ecStats.setClickable(false);
			ceStats.setClickable(true);
			ecStats.setEnabled(false);
			ceStats.setEnabled(true);
			calculateSetStats(name + ".log.txt");
		} else {
			ecStats.setClickable(true);
			ceStats.setClickable(false);
			ecStats.setEnabled(true);
			ceStats.setEnabled(false);
			calculateSetStats(name + ".log.txt");
		}
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ecStats:
			displayStats(EC);
			break;
		case R.id.ceStats:
			displayStats(CE);
			break;
		default:
			break;
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
