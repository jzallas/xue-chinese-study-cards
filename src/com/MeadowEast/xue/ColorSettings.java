package com.MeadowEast.xue;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

public class ColorSettings extends Activity {
	public final static String COLOR = "com.MeadowEast.Xue.COLOR";
	Intent returnIntent = new Intent();
	ImageButton colorOne, colorTwo, colorThree, colorFour, colorFive;
	
	private void setupActionListeners(){
		colorOne = (ImageButton) findViewById(R.id.Color1);
		colorOne.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				requestBGColor(R.color.background_color1);
			}
		});
		
		colorTwo = (ImageButton) findViewById(R.id.Color2);
		colorTwo.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				requestBGColor(R.color.background_color2);
			}
		});
		
		colorThree = (ImageButton) findViewById(R.id.Color3);
		colorThree.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				requestBGColor(R.color.background_color3);
			}
		});
		
		colorFour = (ImageButton) findViewById(R.id.Color4);
		colorFour.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				requestBGColor(R.color.background_color4);
			}
		});
		
		colorFive = (ImageButton) findViewById(R.id.Color5);
		colorFive.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				requestBGColor(R.color.background_color5);
			}
		});
	}
	
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_color_settings);
		setupActionBar();
		setupActionListeners();
		
	}
	
	protected void requestBGColor(int color)
	{
		SharedPreferences prefs = this.getSharedPreferences("com.MeadowEast.xue_preferences", Context.MODE_PRIVATE);
		prefs.edit().putString("bg_color", getResources().getString(color)).commit();
		finish(); //end activity
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
