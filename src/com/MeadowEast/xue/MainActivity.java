package com.MeadowEast.xue;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class MainActivity extends Activity implements OnClickListener {

	Button ecButton, ceButton, exitButton,statButton, downloadButton;
	public static File filesDir;
	public static String mode;
	static final String TAG = "XUE MainActivity";
	static final String PACKAGE = "com.MeadowEast.xue";
	static final int COLOR_CHANGE = 100;
	private boolean enableUpdates;
	private boolean didUpdateOnSunday = false;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setupActionBar();
		downloadButton = (Button) findViewById(R.id.downloadButton);
		ecButton = (Button) findViewById(R.id.ecButton);
		ceButton = (Button) findViewById(R.id.ceButton);
		statButton = (Button)findViewById(R.id.statButton);
		exitButton = (Button) findViewById(R.id.exitButton);
		downloadButton.setOnClickListener(this);
		ecButton.setOnClickListener(this);
		ceButton.setOnClickListener(this);
		exitButton.setOnClickListener(this);
		statButton.setOnClickListener(this);
		File sdCard = Environment.getExternalStorageDirectory();
		filesDir = new File(sdCard.getAbsolutePath()
				+ "/Android/data/com.MeadowEast.xue/files");
		Log.d(TAG, "xxx filesDir=" + filesDir);
		// set default settings on first run
		PreferenceManager.setDefaultValues(this, R.xml.pref_defaults, false);
		loadSettings();
		
		/********************************************************************************************
		 * Automatically check for updates every Sunday if "Updates enabled" are checked in settings *
		 ********************************************************************************************/
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK); 
		// If current day is Sunday, day=1
		if(enableUpdates && day == 1 && didUpdateOnSunday == false){
			Intent i = new Intent(this, downloadActivity.class);
			startActivity(i);
			didUpdateOnSunday = true;
		}
		
	}

	private void loadSettings() {
		SharedPreferences sharedPref = PreferenceManager
				.getDefaultSharedPreferences(this);
		RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.MainLayout);
		mainLayout.setBackgroundColor(Color.parseColor(sharedPref
				.getString("bg_color",
						getResources().getString(R.color.background_color5))));
		//ENABLE OR DISABLE AUTO UPDATES
		enableUpdates = sharedPref.getBoolean("updates_checkbox", false);
	}
	
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		loadSettings();
		// Restore state here
	}

	public void onClick(View v) {
		Intent i;
		switch (v.getId()) {
		case R.id.downloadButton:
			Log.d(TAG, "download button clicked");
			i = new Intent(this, downloadActivity.class);
			startActivity(i);
			break;
		case R.id.ecButton:
			mode = "ec";
			i = new Intent(this, LearnActivity.class);
			startActivity(i);
			break;
		case R.id.statButton:
			i = new Intent(this, StatisticsActivity.class);
			startActivity(i);
			break;
		case R.id.ceButton:
			mode = "ce";
			i = new Intent(this, LearnActivity.class);
			startActivity(i);
			break;
		case R.id.exitButton:
			finish();
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.settings:
			Intent intent_settings = new Intent(this, SettingsActivity.class);
			startActivity(intent_settings);
			break;
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		return super.onOptionsItemSelected(item);
	}
	
}
