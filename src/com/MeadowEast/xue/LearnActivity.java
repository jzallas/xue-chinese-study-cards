package com.MeadowEast.xue;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class LearnActivity extends Activity implements OnClickListener,
		OnLongClickListener, ClipboardManager.OnPrimaryClipChangedListener {
	static final String TAG = "LearnActivity";
	static int ECDeckSize, CEDeckSize, ECTarget, CETarget,
			minutesBetweenDuplicate;

	//timer
	private Chronometer chronometer;
	long timeWhenStopped = 0;

	// Swipe Gesture Detector
	private static final int SWIPE_MIN_DISTANCE = 8; // 90; // 120;
	private static final int SWIPE_MAX_OFF_PATH = 150; // 280 // 200;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	private GestureDetector gestureDetector; // imported GestureDetector
	View.OnTouchListener gestureListener;


	//sound items
    private MediaPlayer mPlayer;
    private boolean soundFx; // , enableUpdates;
	
	LearningProject lp;

	// clipboard monitor
	ClipboardManager clipBoard;

	int itemsShown;
	TextView prompt, answer, other, status;
	Button advance, okay;

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// un-register on pause to prevent android 4.3 bug
		clipBoard.removePrimaryClipChangedListener(this);
		
		timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
		chronometer.stop();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// clear clipdata
		ClipData data = ClipData.newPlainText("", "");
		clipBoard.setPrimaryClip(data);
		// re-register on resume to prevent android 4.3 bug
		clipBoard.addPrimaryClipChangedListener(this);
		
		chronometer.setBase(SystemClock.elapsedRealtime() + timeWhenStopped);
		chronometer.start();

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_learn);
		setupActionBar();
		loadSettings();
		Log.d(TAG, "Entering onCreate");

		clipBoard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
		
		chronometer = (Chronometer) findViewById(R.id.chronometer1);
		chronometer.start();


		gestureDetector = new GestureDetector(this, new MyGestureDetector());
		gestureListener = new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		};

		itemsShown = 0;
		prompt = (TextView) findViewById(R.id.promptTextView);
		status = (TextView) findViewById(R.id.statusTextView);
		other = (TextView) findViewById(R.id.otherTextView);
		answer = (TextView) findViewById(R.id.answerTextView);
		advance = (Button) findViewById(R.id.advanceButton);
		okay = (Button) findViewById(R.id.okayButton);

		// add listeners
		prompt.setOnClickListener(LearnActivity.this);
		prompt.setOnTouchListener(gestureListener);
		status.setOnClickListener(LearnActivity.this);
		status.setOnTouchListener(gestureListener);
		other.setOnClickListener(LearnActivity.this);
		other.setOnTouchListener(gestureListener);
		answer.setOnClickListener(LearnActivity.this);
		answer.setOnTouchListener(gestureListener);

		// findViewById(R.id.advanceButton).setOnClickListener(this);
		// findViewById(R.id.okayButton).setOnClickListener(this);

		findViewById(R.id.promptTextView).setOnLongClickListener(this);
		findViewById(R.id.answerTextView).setOnLongClickListener(this);
		findViewById(R.id.otherTextView).setOnLongClickListener(this);

		if (MainActivity.mode.equals("ec")) {
			lp = new EnglishChineseProject(ECDeckSize, ECTarget,
					minutesBetweenDuplicate);
			other.setTextIsSelectable(true);
		} else {
			lp = new ChineseEnglishProject(CEDeckSize, CETarget,
					minutesBetweenDuplicate);
			prompt.setTextIsSelectable(true);
		}
		clearContent();
		doAdvance();
	}

	public void onPrimaryClipChanged() {
		// do something useful here with the clipboard
		// get clipdata and make url
		String url = "http://www.mdbg.net/chindict/chindict.php?page=worddict&wdrst=0&wdqb="
				+ clipBoard.getText().toString();

		// launch some browser with an implicit intent
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		try {
			startActivity(browserIntent);
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(this, "No browser found",
					Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(false);
		}
	}

	private void loadSettings() {
		// SET BACKGROUND COLOR
		SharedPreferences sharedPref = PreferenceManager
				.getDefaultSharedPreferences(this);
		LinearLayout learnLayout = (LinearLayout) findViewById(R.id.LearnLayout);
		learnLayout.setBackgroundColor(Color.parseColor(sharedPref
				.getString("bg_color",
						getResources().getString(R.color.background_color5))));
		// ENABLE OR DISABLE SOUND
		soundFx = sharedPref.getBoolean("sound_checkbox", true);
		//EDIT DECK SIZES PER SETTINGS
		ECDeckSize = Integer.parseInt(sharedPref.getString("ec_deck_size_list", "40"));
		CEDeckSize = Integer.parseInt(sharedPref.getString("ce_deck_size_list", "60"));
		//EDIT TARGET PER SETTINGS
		ECTarget = Integer.parseInt(sharedPref.getString("ec_target_list", "700"));
		CETarget = Integer.parseInt(sharedPref.getString("ce_target_list", "700"));
		//GET DUPLICATE FREQUENCY
		minutesBetweenDuplicate = Integer.parseInt(sharedPref.getString("duplicate_freq_list", "1440"));
		
	}

	private void doAdvance() {
		Log.d("!", "doAdvance CALLED!");
		if (itemsShown == 0) {
			if (lp.next()) {
				prompt.setText(lp.prompt());
				status.setText(lp.deckStatus());
				itemsShown++;
			} else {
				Log.d(TAG, "Error: Deck starts empty");
				throw new IllegalStateException("Error: Deck starts empty.");
			}
		} else if (itemsShown == 1) {

			answer.setText(lp.answer());
			itemsShown++;
			animateBottomAnswer();
			// playSound();
		} else if (itemsShown == 2) {

			other.setText(lp.other());
			// advance.setText("next");
			itemsShown++;
			animateBottomOther();
			// playSound();
		} else if (itemsShown == 3) {
			// Got it wrong
			// advance.setText("show");
			// lp.wrong();
			// lp.next();
			// clearContent();
			// prompt.setText(lp.prompt());
			// itemsShown = 1;
			// status.setText(lp.deckStatus());
		}
	}

	private void clearContent() {
		prompt.setText("");
		answer.setText("");
		other.setText("");
	}

	
// swiping up - removing card from deck	
	private void doOkay(){
        if (okay.getText().equals("done")) //if done
                try {
                	Log.d(TAG,"DONE");
                	
                	// prompt user to click "done" to end activity and write results...
                	Toast.makeText(this, "Your progress has been saved",
        					Toast.LENGTH_LONG).show();
                	
                    lp.log(lp.queueStatus()); //write to log
                    lp.writeStatus(); //save indexSets and timestamps
                    finish(); //end activity
                    return;
                    //System.exit(0);
                } catch (IOException e) {
                    Log.d(TAG, "couldn't write Status");
                    return;
                }
        // Do nothing unless answer has been seen
        if (itemsShown < 2) return;
        // Got it right
        lp.right();
        if (lp.next()){
        	advance.setText("show");
        	clearContent();
        	prompt.setText(lp.prompt());
        	itemsShown = 1;
        	status.setText(lp.deckStatus());
        	animateTop();
        	playCorrectSound();
        } else {
        	advance.setText("show");
        	clearContent();
        	status.setText("Swipe up to save the progress to disk.\nSwipe right to undo.");
        	prompt.setText("Congratulations!\nYou have reached end of deck.");
        	answer.setText("");
        	okay.setText("done");
//        	okay.setVisibility(View.GONE);
            
        	itemsShown = 2;
        	playCorrectSound();
        }
//        else {
//        	Log.d("!","INSIDE ELSE");
//        	((ViewManager) advance.getParent()).removeView(advance);
//        	status.setText("");
//        	okay.setText("done");
//        	clearContent();
//        	animateTop();
//        	playCorrectSound();
//        }

	}

	/*******************************************************************
	 * TranslateAnimation transAnimation= new TranslateAnimation(fromXposition,
	 * toXPosition, fromYPosition, toYPosition);
	 * 
	 * //fromXposition- x coordinate from where animation should start
	 * //toXPosition- x coordinate at which animation would end //fromYPosition-
	 * y coordinate from where animation should start. //toYPosition- y
	 * coordinate at which animation would end.
	 * 
	 * //You can also set duration for the animation that means you can set for
	 * how long the animation should last: transAnimation.setDuration(550);
	 * //You can now apply the animation to a view
	 * prompt.startAnimation(transAnimation);
	 ********************************************************************/

	private void animateTop() {
		TranslateAnimation transAnimation = new TranslateAnimation(0, 0, 200, 0);
		transAnimation.setDuration(100);
		prompt.startAnimation(transAnimation);
	}

	private void animateBottomAnswer() {
		TranslateAnimation transAnimation = new TranslateAnimation(0, 0, -200,
				0);
		transAnimation.setDuration(100);
		answer.startAnimation(transAnimation);
		// other.startAnimation(transAnimation);
	}

	private void animateBottomOther() {
		TranslateAnimation transAnimation = new TranslateAnimation(0, 0, -200,
				0);
		transAnimation.setDuration(100);
		other.startAnimation(transAnimation);
	}

	private void animateLeft() {
		TranslateAnimation transAnimation = new TranslateAnimation(500, 0, 0, 0);
		transAnimation.setDuration(100);
		prompt.startAnimation(transAnimation);
	}

	private void animateRight() {
		TranslateAnimation transAnimation = new TranslateAnimation(-500, 0, 0,
				0);
		transAnimation.setDuration(100);
		prompt.startAnimation(transAnimation);
		answer.startAnimation(transAnimation);
		other.startAnimation(transAnimation);
	}

	public void onClick(View v) {
		// if(v.getId() == R.id.advanceButton && okay.getText() == "done"){
		// Log.d(TAG, "okay button clicked ---> DONE");
		// doOkay();
		// }

		// Filter f = (Filter) v.getTag();
		// FilterFullScreenActivity.show(this, input, f);
		// switch (v.getId()) {
		// case R.id.advanceButton:
		// doAdvance();
		// break;
		// case R.id.okayButton:
		// Log.d(TAG, "okay button clicked ---> DONE");
		// doOkay();
		//
		// break;
		// case R.id.promptTextView:
		// case R.id.answerTextView:
		// case R.id.otherTextView:
		// Toast.makeText(this, "Item index: "+lp.currentIndex(),
		// Toast.LENGTH_LONG).show();
		// break;

		// default :
		// // doAdvance();
		// break;
		// }
	}
	
	void reportError(){
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setType("message/rfc822");
		emailIntent.putExtra(Intent.EXTRA_EMAIL,
				new String[] { "xue@meadoweast.com" });
		emailIntent.putExtra(Intent.EXTRA_SUBJECT,
				"ERROR REPORT: ITEM INDEX : " + lp.currentIndex());
		emailIntent.putExtra(Intent.EXTRA_TEXT,
				"I am reporting an error from " + MainActivity.PACKAGE + " application with the following card :\n" + lp.prompt() + "\n"
						+ lp.answer() + "\n" + lp.other());
		try {
			startActivity(Intent.createChooser(emailIntent, "Send Error Report"));
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(this, "Item index: " + lp.currentIndex(),
					Toast.LENGTH_LONG).show();
		}
	}

	public boolean onLongClick(View v) {
		switch (v.getId()) {
		case R.id.promptTextView:
		case R.id.answerTextView:
		case R.id.otherTextView:
			Toast.makeText(this, "Sending Error Report",
					Toast.LENGTH_SHORT).show();
			reportError();
			break;
		}
		return true;
	}

	private void onSwipeTop() { // throws IllegalArgumentException,
								// SecurityException, IllegalStateException,
								// IOException {
		// TODO Auto-generated method stub
		Log.d(TAG, "onSwipeTop()");
		doOkay();
	}

	private void onSwipeBottom() {
		if (okay.getText().equals("done")) // to prevent swiping bottom when end
											// of deck reached
			return;
		// TODO Auto-generated method stub
		Log.d(TAG, "onSwipeBottom()");
		doAdvance();
	}

	private void onSwipeLeft() {
		if (okay.getText().equals("done")) // to prevent swiping left when end
											// of deck reached
			return;
		// TODO Auto-generated method stub
		Log.d(TAG, "onSwipeLeft()");

		// code from old doAdvance() function --> condition: (itemsShown == 3)
		if (itemsShown > 1) {
			animateLeft();
			lp.wrong();
			lp.next();
			clearContent();
			prompt.setText(lp.prompt());
			itemsShown = 1;
			status.setText(lp.deckStatus());
			okay.setText("show");
			playWrongSound();
		}
	}

	private void onSwipeRight() {
		// TODO Auto-generated method stub
		Log.d(TAG, "onSwipeRight()");
		okay.setText("show");
		if (lp.undo()) {
			animateRight();
			Log.d(TAG, "Undo action successful");
			clearContent();
			prompt.setText(lp.prompt());
			status.setText(lp.deckStatus());
			itemsShown = 1;
		} else {
			Log.d(TAG, "SHOULD SEE TOAST");
			Toast.makeText(this, "Cannot undo anymore", Toast.LENGTH_SHORT)
					.show();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Log.d(TAG, "llkj");
			new AlertDialog.Builder(this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle(R.string.quit)
					.setMessage(R.string.reallyQuit)
					.setPositiveButton(R.string.yes,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									LearnActivity.this.finish();
								}
							}).setNegativeButton(R.string.no, null).show();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	class MyGestureDetector extends SimpleOnGestureListener { // imported
																// SimpleOnGestureListener
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			try {
				// Log.d("!", "e1 - e2 -> X " + Math.abs(e1.getX() -
				// e2.getX()));
				// Log.d("!", "e1 - e2 -> Y " + Math.abs(e1.getY() -
				// e2.getY()));
				// Log.d("!", "velocityX " + velocityX);
				// Log.d("!", "velocityY " + velocityY);
				if (Math.abs(velocityY) > Math.abs(velocityX)) {
					// if (Math.abs(e1.getY() - e2.getY()) >
					// SWIPE_MAX_OFF_PATH){
					if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE // ){
							&& Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {

						onSwipeTop();

					} else // if(e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE
							// && Math.abs(velocityX) >
							// SWIPE_THRESHOLD_VELOCITY){
					{
						onSwipeBottom();
					}
					// return false;
				}
				// right to left swipe
				else if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE // ){
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					// Toast.makeText(LearnActivity.this, "Left Swipe",
					// Toast.LENGTH_SHORT).show();
					onSwipeLeft();

				} else { // if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE){
					// && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					// Toast.makeText(LearnActivity.this, "Right Swipe",
					// Toast.LENGTH_SHORT).show();
					onSwipeRight();
				}
			} catch (Exception e) {
				// nothing
			}
			return false;
		}
	}

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
	
	  public void playCorrectSound(){
	        if(soundFx==true){
	                mPlayer = MediaPlayer.create(this, R.raw.correct); 
	                mPlayer.start();
	                if (!mPlayer.isPlaying()){
	                 mPlayer.release();
	                  }
	                }
	        
	    }
	  public void playWrongSound(){
	        if(soundFx==true){
	                mPlayer = MediaPlayer.create(this, R.raw.wrong);
	                mPlayer.start();
	                }
	        if (!mPlayer.isPlaying()){
	            mPlayer.release();
	             }
	        
	    }

}
