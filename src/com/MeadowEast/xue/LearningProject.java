package com.MeadowEast.xue;

import java.io.*;
//import java.text.DateFormat;
//import java.text.ParseException;
import java.util.*;

import android.util.Log;

abstract public class LearningProject {
	
	private String name;
	private int n, seen;
	protected List<IndexSet> indexSets;
	protected Map<Integer, Date> timestamps;
	protected Deck deck;
	protected undoDeck undoDeck;	
	protected CardStatus cardStatus = null;
	protected Card card = null;	
	final static String TAG = "CC LearningProject";
	protected float hoursBetweenDuplicates;
	
	public LearningProject(String name, int n, int target, int minutesBetweenDuplicates) {
		hoursBetweenDuplicates = (float)minutesBetweenDuplicates/(float)60;
		this.n = n;
		this.name = name;
		this.seen = 0;
		Log.d(TAG, "Creating index sets");
		indexSets = new ArrayList<IndexSet>();
		for (int i=0; i<5; ++i){
			indexSets.add(new IndexSet());
		}
		timestamps = new HashMap<Integer, Date>();
		Log.d(TAG, "Reading status");
		readStatus();
		Log.d(TAG, "Making deck");
		deck = makeDeck(n, target);
		undoDeck = makeUndoDeck();
		Log.d(TAG, "Exiting LearningProject constructor");
	}	
	
	private undoDeck makeUndoDeck()
	{
		undoDeck u = new undoDeck();
		return u;
	}

	// n is the size of the deck
	// target is used to limit the number at Levels 1 and 2, the ones
	//   mainly being learned. The higher target is, the higher probability of level 1 and 2
 	private Deck makeDeck(int n, int target){
 		Date now = new Date();
		Random r = new Random();
		Deck d = new Deck();
		float[] cutoffs = new float[4];
		// Set cutoffs so that new cards are only introduced so as to maintain a limit
		// of target at Level 1 and 2*target at (Level 1 + Level 2).  We always spend
		// 50 percent of our time on Level 1 (and 7 percent and 3 percent each on Levels
		// 3 and 4).  The remaining 40 percent gets divided between Levels 0 and 2, with
		// Level 0 time approaching zero as the in-play number gets to 2*target
		
		// Note: When Level 1 and Level 2 are both half full, this cuts the time on Level 0
		// to 12.5 percent, which is kind of low.  Easy to fix by setting target higher.
		float factor1 = Math.max(0,  1-indexSets.get(1).size()/(float) target);
		float factor1n2 = Math.max(0, 1-(indexSets.get(1).size()+indexSets.get(2).size())/((float) 2*target));
		cutoffs[0] = .40f * factor1 * factor1n2;
		if (cutoffs[0] < 0) cutoffs[0] = 0f;
		cutoffs[1] = cutoffs[0] + .5f;
		cutoffs[2] = .90f;
		cutoffs[3] = .97f;
		if (indexSets.get(0).size() < n)
			addNewItems(n);
		while (d.size()<n){
			double x = r.nextFloat();
			int level;
			if (x < cutoffs[0]){
				level = 0;
			} else if (x < cutoffs[1]) {
				level = 1;
			} else if (x < cutoffs[2]) {
				level = 2;
			} else if (x < cutoffs[3]) {
				level = 3;
			} else {
				level = 4;
			}
			IndexSet targetSet = indexSets.get(level);
			if (targetSet.size() > 0){
				int index = targetSet.pickOne();
				// add it if it hasn't recently been seen
				long hoursSinceSeen = (now.getTime() - timestamps.get(index).getTime()) / (60 * 60 * 1000);
				if ((float)hoursSinceSeen > hoursBetweenDuplicates){
					timestamps.put(index, now);
					d.put(new CardStatus(index, level));
				} else { // if it's too recent, put it back
					indexSets.get(level).add(index);
				}
			}
		}
		return d;
	}
	
 	
	public boolean next() {
		if (deck.isEmpty()) return false;
		cardStatus = deck.get();
		seen++;
		card = AllCards.getCard(cardStatus.getIndex());
		return true;
	}
	
	public int currentIndex(){
		if (cardStatus==null)
			return -1;
		return cardStatus.getIndex();
	}
	
	abstract protected String prompt();
	abstract protected String answer();
	abstract protected String other();
	abstract public void addNewItems();
	abstract public void addNewItems(int n);
	
	public void right(){
		undoDeck.push(cardStatus); // push to undostack before touching indexSets
		cardStatus.right();
		// put it in the appropriate index set
		indexSets.get(cardStatus.getLevel()).add(cardStatus.getIndex());
		
	}
	
	public void wrong(){
		undoDeck.push(cardStatus);
		cardStatus.wrong();
		deck.put(cardStatus);
	}
	
	protected boolean undo(){
		Log.d(TAG, "UNDO CALLED") ;
		if(undoDeck.isEmpty()){ // nothing to undo here
			Log.d(TAG, "undo deck is EMPTY! Cannot undo anymore") ;
			return false; 
		}
		if(cardStatus != null) // put cardstatus back into first place of deck
			deck.insertInHead(cardStatus);
		// wrong() was the last action before undo was called:
		cardStatus = undoDeck.pop(); 
		if(deck.contains(cardStatus)) {
			Log.d(TAG, "WRONG was the previous action") ;
			deck.removeDuplicate(cardStatus);
			--seen;
			card = AllCards.getCard(cardStatus.getIndex());
			return true;
		}

		// right() was the last action before undo was called:
		else{ 
			Log.d(TAG, "RIGHT was the previous action") ;
			--seen;
			// fix indexSets when right was last action
			// reverse the action when right() was clicked
			if(cardStatus.getLevel() < 4 )
				indexSets.get(cardStatus.getLevel()+1).remove(cardStatus.getIndex());
			else // edge case at 4th level
				indexSets.get(cardStatus.getLevel()).remove(cardStatus.getIndex());
			card = AllCards.getCard(cardStatus.getIndex());
		    return true;
		} 
	}
		
	String deckStatus(){
		String left = (deck.size()+1)+" left";
		return seen > n ? left : seen + " of " + n + " seen, " + left; 
	}
	
	String queueStatus(){
		int [] n = new int[5];
		for (int i=0; i<5; ++i) n[i] = indexSets.get(i).size();
//		return String.format("  %7d  %4d  %4d  %4d  %4d  %7d  %5d  ", n[0], n[1], n[2], n[3], n[4],
//				n[2]+n[3]+n[4], n[0]+n[1]+n[2]+n[3]+n[4]);
		return String.format("    %d   %d + %d = %d    %d + %d = %d    %d",
				n[0], n[1], n[2], n[1]+n[2], n[3], n[4], n[3]+n[4], n[0]+n[1]+n[2]+n[3]+n[4]);
		//     86   52 + 905 = 957    999 + 648 = 1647    2690
		//     L0   L1 + L2            L3 + L4             L0-L4
	}
	
	public void log(String s) throws IOException {
		Log.d(TAG, "Entering log okay");
		boolean append = true;
		File logfilehandle = new File(MainActivity.filesDir, name + ".log.txt"); //set the file to the <name>.log.txt - for example, ChineseEnglish.log.txt
		Log.d(TAG, "logfilehandle is: " +logfilehandle);
		FileWriter logfile = new FileWriter(logfilehandle, append);
		PrintWriter out = new PrintWriter(logfile);
		Date now = new Date();
		out.printf("%tD %tR %s\n", now, now, s);
		//03/03/13 10:19 STRING
		logfile.close();
		Log.d(TAG, "Exiting log okay");
	}
	
	public void writeStatus() throws IOException {
		File statusobjectfile = new File(MainActivity.filesDir, name + ".status.ser");
		FileOutputStream statusobjectFOS = new FileOutputStream(statusobjectfile);
		ObjectOutputStream statusobjectOOS = new ObjectOutputStream(statusobjectFOS);
		
		Log.d(TAG, "writing objects");
		statusobjectOOS.writeObject(timestamps);
		statusobjectOOS.writeObject(indexSets);
		statusobjectFOS.close();
	}
	
	@SuppressWarnings("unchecked")
	private void readStatus() {
		FileInputStream statusobjectFIS;
		ObjectInputStream statusobjectOIS;
		try {
			File statusobjectfile = new File(MainActivity.filesDir, name + ".status.ser");
			statusobjectFIS = new FileInputStream(statusobjectfile);
			statusobjectOIS = new ObjectInputStream(statusobjectFIS);
		} catch (Exception e) {
			Log.d(TAG, "No status file, adding 50 first items from AllCards");
			addNewItems(50);
			return;
		} 
		try {
			timestamps = (Map<Integer, Date>) statusobjectOIS.readObject();
			indexSets = (List<IndexSet>) statusobjectOIS.readObject();
			statusobjectFIS.close();
			Log.d(TAG, "OBJECT status file read without problems");
		} catch (Exception e) { Log.d(TAG, "Error in readStatus"); }
	}

}
